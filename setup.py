from distutils.core import setup
import json

with open('package.json', 'r') as f:
    package_json = json.load(f)

version = package_json['version']

setup(
    name = 'scylla-spree-xero',
    packages = ['scylla_spree_xero'],
    version = version,

    description = 'Scylla-Spree-Xero provides the basic utilities to integrate with the standard spree & Xero API',

    #author = '',
    #author_email = '',

    url = 'https://git@gitlab.com:5stones/scylla-spree-xero',
    download_url = 'https://gitlab.com/5stones/scylla-spree-xero/repository/archive.tar.gz?ref=' + version,

    keywords = 'integration scylla spree xero',

    classifiers=[
        #'Development Status :: 4 - Beta',
        #'Development Status :: 5 - Production/Stable',

        #'Intended Audience :: Developers',
        #'Topic :: Software Development :: Build Tools',

        'License :: OSI Approved :: MIT License',

        'Programming Language :: Python :: 2',
        'Programming Language :: Python :: 2.7',
    ],

    install_requires = [
        'scylla',
        'scylla-xero',
        'scylla-spree',
    ],
    dependency_links=[
        'git+https://gitlab.com/5stones/scylla.git',
        'git+https://gitlab.com/5stones/scylla-xero.git',
        'git+https://gitlab.com/5stones/scylla-spree.git',
    ],
)
