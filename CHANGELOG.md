# [1.1.0](https://gitlab.com/5stones/scylla-spree-xero/compare/v1.0.3...v1.1.0) (2019-04-30)


### Features

* **ToXeroLineItem:** Line items need to adjust any rounding discrepencies before being pushed ([03b88f6](https://gitlab.com/5stones/scylla-spree-xero/commit/03b88f6))



## [1.0.3](https://gitlab.com/5stones/scylla-spree-xero/compare/v1.0.2...v1.0.3) (2019-04-09)


### Bug Fixes

* **ToXeroCreditNote:** Fix issue with guest checkouts not having a user_id ([caa334e](https://gitlab.com/5stones/scylla-spree-xero/commit/caa334e))



## [1.0.2](https://gitlab.com/5stones/scylla-spree-xero/compare/v1.0.1...v1.0.2) (2019-01-18)


### Bug Fixes

* **requirements.txt, setup.py:** Remove unused package from deps ([ac0e916](https://gitlab.com/5stones/scylla-spree-xero/commit/ac0e916))



## [1.0.1](https://gitlab.com/5stones/scylla-spree-xero/compare/v1.0.0...v1.0.1) (2018-11-28)


### Bug Fixes

* **requirements.txt:** Fix dependency urls ([7839337](https://gitlab.com/5stones/scylla-spree-xero/commit/7839337))



# [1.0.0](https://gitlab.com/5stones/scylla-spree-xero/compare/9e742b5...v1.0.0) (2018-11-28)


### Bug Fixes

* **app:** disable sync of payments to allow for manual deposits ([488136f](https://gitlab.com/5stones/scylla-spree-xero/commit/488136f))
* **app:** fix filter which excludes $0 orders ([979a9ad](https://gitlab.com/5stones/scylla-spree-xero/commit/979a9ad))
* **app:** fix syntax ([eccc68f](https://gitlab.com/5stones/scylla-spree-xero/commit/eccc68f))
* **app:** make tasks run less regularly ([d4a8747](https://gitlab.com/5stones/scylla-spree-xero/commit/d4a8747))
* **app:** missing comma ([24164e0](https://gitlab.com/5stones/scylla-spree-xero/commit/24164e0))
* **app, conversions:** update criteria to prevent syncing of certain records ([7cc0cc9](https://gitlab.com/5stones/scylla-spree-xero/commit/7cc0cc9))
* **conversions:** change amount not matching error to a warning ([ebb3168](https://gitlab.com/5stones/scylla-spree-xero/commit/ebb3168))
* **conversions:** fix accessing of price ([c203a3c](https://gitlab.com/5stones/scylla-spree-xero/commit/c203a3c))
* **conversions:** fix date to be the completed date instead of creation date ([a630b4e](https://gitlab.com/5stones/scylla-spree-xero/commit/a630b4e))
* **conversions:** fix handling of orders placed with a guest checkout ([c72bd48](https://gitlab.com/5stones/scylla-spree-xero/commit/c72bd48))
* **conversions:** fix invoice date since it's paid when it's shipped ([aa18fe5](https://gitlab.com/5stones/scylla-spree-xero/commit/aa18fe5))
* **conversions:** fix line level discounts ([9dda03d](https://gitlab.com/5stones/scylla-spree-xero/commit/9dda03d))
* **conversions:** fix positive adjustment calculation ([42ecd22](https://gitlab.com/5stones/scylla-spree-xero/commit/42ecd22))
* **conversions:** fix shipping account code ([9775f53](https://gitlab.com/5stones/scylla-spree-xero/commit/9775f53))
* **conversions:** remove item description which is causing errors ([86a7618](https://gitlab.com/5stones/scylla-spree-xero/commit/86a7618))
* **Invoice:** Fix issue with empty string LineAmount ([d72fe5f](https://gitlab.com/5stones/scylla-spree-xero/commit/d72fe5f))
* **Invoice:** Fix issue with get returning empty string ([5045f08](https://gitlab.com/5stones/scylla-spree-xero/commit/5045f08))
* **Invoice:** fix python being picky about operand types ([ed07907](https://gitlab.com/5stones/scylla-spree-xero/commit/ed07907))
* **Invoice:** Remove LineAmount from LineItem if 0, let Xero infer it ([94e2e7d](https://gitlab.com/5stones/scylla-spree-xero/commit/94e2e7d))
* **item:** remove all non-ascii characters when pushing into Xero ([9449b01](https://gitlab.com/5stones/scylla-spree-xero/commit/9449b01))


### Features

* **app:** add recurring task to capture Spree invoices paid in Xero ([02c8f73](https://gitlab.com/5stones/scylla-spree-xero/commit/02c8f73)), closes [#1](https://gitlab.com/5stones/scylla-spree-xero/issues/1)
* **app:** stop updating invoices because paid invoices cannot be updated ([a0485c0](https://gitlab.com/5stones/scylla-spree-xero/commit/a0485c0))
* **app, conversions:** add task for syncing refunds to xero ([c87f7a9](https://gitlab.com/5stones/scylla-spree-xero/commit/c87f7a9)), closes [#3](https://gitlab.com/5stones/scylla-spree-xero/issues/3)
* **app, conversions:** change item sync to use variants instead of products ([9e742b5](https://gitlab.com/5stones/scylla-spree-xero/commit/9e742b5))
* **conversions:** add config element for the account used on payments ([f387e7d](https://gitlab.com/5stones/scylla-spree-xero/commit/f387e7d))
* **conversions:** add option to remove LineAmount and leave it to Xero ([7ff12e2](https://gitlab.com/5stones/scylla-spree-xero/commit/7ff12e2))
* **LineItem:** Add dollar amount to markup ([5694aa8](https://gitlab.com/5stones/scylla-spree-xero/commit/5694aa8))
* **LineItem:** add positive adjustments to unit price ([87b3787](https://gitlab.com/5stones/scylla-spree-xero/commit/87b3787))
* **LineItem:** subtract tax from spree calculated total ([49076f4](https://gitlab.com/5stones/scylla-spree-xero/commit/49076f4))
* **LineItem:** update line item amount & description calculations ([0b23b33](https://gitlab.com/5stones/scylla-spree-xero/commit/0b23b33))
* **setup.*, package.json, README.md, LICENSE:** Add package install files ([8188ee9](https://gitlab.com/5stones/scylla-spree-xero/commit/8188ee9))



