from scylla import tasks
import scylla_spree as spree
import scylla_xero as xero


class ToXeroTask(xero.ToXeroTask):
    def __init__(self,
            spree_client,
            xero_client_collection,
            from_class,
            conversion,
            to_class,
            where=None, with_reflection=None):

        super(ToXeroTask, self).__init__(
            xero_client_collection,
            from_class,
            conversion,
            to_class,
            where=where,
            with_reflection=with_reflection)

        self.spree_client = spree_client

    def _process_response_record(self, obj):
        spree_obj = spree.records.SpreeRecord.factory(self.spree_client, self.from_type, obj)
        return super(ToXeroTask, self)._process_response_record(spree_obj)
