"""Integrates spree with xero accounting.
It will query the database, and use that result to push records into the other system.

configuration options:
    'Xero': {
        'item_account': 400,
        'shipping_account': 400,
        'payment_account': 400,
        'validate_amount': False,
        'line_amounts': True,
    },
"""

from .app import App

from .conversions import ToXeroAddress, ToXeroContact, ToXeroInvoiceContact, ToXeroLineItem, ToXeroShipment, ToXeroInvoice, ToXeroPayment, ToXeroItem, ToXeroCreditNote
