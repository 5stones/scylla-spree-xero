from scylla import app
import scylla_spree as spree
import scylla_xero as xero
from . import conversions
from .tasks import ToXeroTask


class App(app.App):
    # wait 10 minutes between each set of tasks, because of usage limits
    nap_length = 600

    def _prepare(self):
        self.spree_client = spree.SpreeRestClient()
        self.xero_client = xero.XeroClient()

        # updates linked contacts; contacts are created with the invoice
        self.on_demand_tasks['spree-user'] = ToXeroTask(
            self.spree_client,
            self.xero_client.contacts,
            (self.spree_client.name, 'User'),
            conversions.ToXeroContact(),
            (self.xero_client.name, 'Contact'),
            # they have to have a billing address
            where=("bill_address IS NOT NULL"),
            # only users linked to contacts
            with_reflection=True)


        # sync items as xero untracked items
        self.tasks['spree-item'] = ToXeroTask(
            self.spree_client,
            self.xero_client.items,
            (self.spree_client.name, 'Variant'),
            conversions.ToXeroItem(),
            (self.xero_client.name, 'Item'),
            # don't create items without a sku or inactive ones
            where=("sku != '' AND deleted_at IS NULL"),
            # only create items, don't update them
            with_reflection=False)

        # pushes in an invoice and its contact
        self.tasks['spree-order'] = ToXeroTask(
            self.spree_client,
            self.xero_client.invoices,
            (self.spree_client.name, 'Order'),
            conversions.ToXeroInvoice(),
            (self.xero_client.name, 'Invoice'),
            # only sync orders that are shipped, since those are the
            # ones that need to be paid or are paid
            where=("completed_at IS NOT NULL "
                "AND total.asFloat() != 0 "
                "AND payment_state IS NOT NULL "
                "AND (shipment_state = 'shipped' OR payment_state = 'paid')"),
            # cannot update paid invoices in Xero
            with_reflection=False)

        # pushes in a credit note for a refund
        self.tasks['spree-refund'] = ToXeroTask(
            self.spree_client,
            self.xero_client.creditnotes,
            (self.spree_client.name, 'Refund'),
            conversions.ToXeroCreditNote(),
            (self.xero_client.name, 'CreditNote'),
            # refunds can't be updated
            with_reflection=False)

        # applies a payment to a Xero invoice
        self.on_demand_tasks['spree-payment'] = (ToXeroTask(
            self.spree_client,
            self.xero_client.payments,
            (self.spree_client.name, 'Payment'),
            conversions.ToXeroPayment(),
            (self.xero_client.name, 'Payment'),
            # only completed credit card payments
            where=("_parent.completed_at IS NOT NULL "
                #"AND source_type = 'Spree::CreditCard' "
                "AND state = 'completed' "),
            # cannot update payments in Xero
            with_reflection=False)
            # include the invoice of the order, which this will apply to
            .with_connection('_parent', self.xero_client.name+'Invoice',
                '_InvoiceID', None, 'InvoiceID')
            )

        # completes invoice payments in Spree when fully paid in Xero
        self.tasks['capture-invoices'] = spree.CaptureSpreeInvoicesTask(
            from_class=(self.xero_client.name, 'Invoice'),
            client=self.spree_client,
            where=('FullyPaidOnDate IS NOT NULL') )

        # TODO add task set the status of voided payments to "DELETED"
        # TODO add task to create refunds in xero
