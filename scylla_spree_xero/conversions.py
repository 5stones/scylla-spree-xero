import decimal
import re
from decimal import Decimal
from pprint import pprint
from scylla import convert
from scylla import configuration
from scylla import log
from scylla import orientdb
import scylla_xero as xero

configuration.setDefaults('Xero', {
    'item_account': 400,
    'shipping_account': 400,
    'payment_account': 400,
    'validate_amount': False,
    'line_amounts': True,
    'debug': False,
})


class _ToXeroConfigConversion(xero.XeroConversion):

    config = configuration.getSection('Xero')


class _ToXeroTransaction(_ToXeroConfigConversion):

    @staticmethod
    def _add_extra_lines(order, obj):
        config = configuration.getSection('Xero')

        # add lines for shipping cost
        for shipment in order.get('shipments'):
            shipment_conv = ToXeroShipment()
            obj['LineItems'].append(shipment_conv(shipment))

        # calculate total adjustments on the order level
        tax_adjustments = Decimal()
        order_discount = Decimal()
        adjustment_labels = []
        for adjustment in order.get('adjustments', []):
            if adjustment.get('eligible'):
                if adjustment.is_tax:
                    tax_adjustments += adjustment['amount']
                else:
                    order_discount += adjustment['amount']
                    adjustment_labels.append(adjustment.get('label'))

        # add one line for order level adjustments
        if tax_adjustments != 0.00 or order_discount != 0.00:
            obj['LineItems'].append({
                'Description': 'Adjustments : {}'.format(
                    ', '.join(adjustment_labels)),
                'AccountCode': config['item_account'],
                'UnitAmount': order_discount,
                'TaxType': 'OUTPUT',
                'TaxAmount': tax_adjustments,
                'LineAmount': order_discount,
            })

        # clean up lines and calculate total
        total = Decimal()
        for line in obj['LineItems']:
            total += line.get('LineAmount', Decimal()) + line['TaxAmount']
            # prevent errors related to discounts percentages
            if not line.get('LineAmount') or not config.get('line_amounts'):
                # let xero calculate the total
                del line['LineAmount']

        # verify totals
        if total != order['total']:
            message = "Order total does not match the sum of lines {} != {}".format(
                total, order['total'])
            if config['validate_amount']:
                raise Exception(message)
            else:
                log.print_error("{}: {}".format(order['number'], message))

        if config.get('debug'):
            print('\n### Converted {}: ###\n'.format(order.get('number')))
            pprint(obj)

        return obj


class ToXeroAddress(xero.XeroConversion):

    def __init__(self):
        key_field = None
        super(ToXeroAddress, self).__init__(key_field)

        self.conversion_map.update({
            'AttentionTo': ('full_name', ),
            'AddressType': u'STREET', # STREET or POBOX
            'City': ('city', ),
            'Country': ('country', 'iso'),
            'PostalCode': ('zipcode', ),
            'Region': ('state_text', ),
        })

    def _post_convert(self, obj, obj_out):
        lines = []
        for f in ['company', 'address1', 'address2']:
            line = obj.get(f)
            if line:
                lines.append(line)
        for (i, line) in enumerate(lines):
            line_field = 'AddressLine{}'.format(i + 1)
            obj_out[line_field] = line

        return obj_out


class ToXeroContact(_ToXeroConfigConversion):

    def __init__(self):
        key_field = 'ContactID'
        super(ToXeroContact, self).__init__(key_field)

        self.conversion_map.update({
            # use any existing contact with this email address
            'ContactID': ('email', self._lookup_contact_id, ),
            'ContactNumber': ('id', self._format_contact_number, ),
            'Name': (self._convert_entity_name, ),
            'FirstName': ('bill_address', 'firstname', ),
            'LastName': ('bill_address', 'lastname', ),
            'Phones': ('bill_address', 'phone', self._parse_phone, ),
            'EmailAddress': ('email', ),
            'Addresses': (self._convert_addressbook, ),
        })

    @staticmethod
    def _lookup_contact_id(email):
        """Find a Xero ContactID by email address.
        """
        classname = ToXeroContact.config.get('name', 'Xero') + 'Contact'
        return orientdb.lookup(classname, 'EmailAddress', email, 'ContactID', None)

    @staticmethod
    def _format_contact_number(user_id):
        return 'S-{}'.format(user_id)[:50]

    @staticmethod
    def _convert_entity_name(rec):
        """Generate a unique name for a user. (in the future it may not have to be unique)
        """
        bill_address = rec.get('bill_address')
        if not bill_address:
            raise Exception("Missing billing address.")

        return '{} <{}>'.format(bill_address.get('full_name'), rec.get('email'))

    @staticmethod
    def _parse_phone(phone):
        return [{
                'PhoneType': 'DEFAULT',
                'PhoneNumber': phone,
                'PhoneAreaCode': '',
                'PhoneCountryCode': '',
        }]

    @staticmethod
    def _convert_addressbook(rec):
        book = []

        bill_address = rec.get('bill_address')
        if bill_address:
            address = ToXeroAddress()(bill_address)
            # POBOX is the invoice address "Postal Address"
            address['AddressType'] = 'POBOX'
            book.append(address)

        ship_address = rec.get('ship_address')
        if ship_address:
            address = ToXeroAddress()(ship_address)
            # STREET is the "Street Address"
            address['AddressType'] = 'STREET'
            book.append(address)

        return book


class ToXeroInvoiceContact(ToXeroContact):

    def __init__(self):
        super(ToXeroInvoiceContact, self).__init__()

        self.conversion_map.update({
            # use email for guest checkout since there won't be a user_id
            'ContactNumber': (
                lambda inv: inv.get('user_id', inv.get('email')),
               self._format_contact_number,
           ),
        })


class ToXeroLineItem(_ToXeroConfigConversion):

    def __init__(self):
        key_field = 'LineItemID'
        super(ToXeroLineItem, self).__init__(key_field)

        self.conversion_map.update({
            #'LineItemID': ,
            'Description': (self._normalize_description, ),

            'ItemCode': ('variant', 'sku', ToXeroItem.normalize_sku, ),
            #'Tracking': ,

            #'AccountCode': (
            #    lambda i: self.config['item_account'], ),
            #'TaxType': 'OUTPUT',

            'Quantity': ('quantity', ),
            'UnitAmount': (lambda line_item: line_item.get('price')
                    + (line_item.total_markup / line_item.get('quantity')), ),
            'TaxAmount': ('.total_tax', ),
            'DiscountRate': ('.discount_rate_with_markup', ),
            'LineAmount': (lambda line_item: line_item.get('total') - line_item.total_tax, ),
        })

    def _post_convert(self, obj, result):
        result = self._adjust_rounding_discrepencies(result)
        return result

    def _adjust_rounding_discrepencies(self, line_item):
        # FYI: Xero's method for rounding is the following:
        # some_amount.quantize(Decimal('.01'), rounding=decimal.ROUND_HALF_UP)
        amount = Decimal(line_item.get('UnitAmount', 1))
        quantity = Decimal(line_item.get('Quantity', 1))
        total = Decimal(line_item.get('LineAmount', 0))
        discount_rate = Decimal(line_item.get('DiscountRate', 0))
        adjusted_discount_rate = discount_rate.quantize(Decimal('.01'), rounding=decimal.ROUND_HALF_UP)

        discount = Decimal(1) - (Decimal(adjusted_discount_rate) / Decimal('100'))
        line_amount = (amount * quantity * discount)
        line_amount = line_amount.quantize(Decimal('.01'), rounding=decimal.ROUND_HALF_UP)

        # default total/LineAmount if it's been removed (use raw discount_rate)
        if total == Decimal('0'):
            total = amount * quantity * (Decimal(1) - (discount_rate / Decimal('100')))

        if adjusted_discount_rate != discount_rate:
            # solve for delta in: `total = (amount + delta) * quantity * discount`
            delta = (total / quantity / discount) - amount
            adjusted_amount = amount + delta

            if self.config.get('debug', False):
                print('{} = ({} / {} / {}) - {}'.format(delta, total, quantity, discount, amount))
                print('Old Unit Amount: {} New Unit Amount: {}'.format(amount, adjusted_amount))

            # adjust discount rate and unit amount
            line_item['DiscountRate'] = adjusted_discount_rate
            line_item['UnitAmount'] = adjusted_amount

        return line_item

    @staticmethod
    def _normalize_description(line_item):
        full_name = line_item.get('variant').full_name

        for adjustment in line_item.get('adjustments'):
            if adjustment.is_markup:
                full_name = '{} : Markup({} - ${})'.format(full_name, adjustment.get('label'), adjustment.get('amount'))

        return full_name


class ToXeroShipment(_ToXeroConfigConversion):

    def __init__(self):
        key_field = 'LineItemID'
        super(ToXeroShipment, self).__init__(key_field)

        self.conversion_map.update({
            #'LineItemID': ,
            'Description': ('selected_shipping_rate', 'name',
                lambda name: 'Shipping : ' + name),

            #'ItemCode': ,
            'AccountCode': (
                lambda s: self.config['shipping_account'], ),
            #'Tracking': ,

            'UnitAmount': ('cost', ),

            'TaxType': 'OUTPUT',
            'TaxAmount': ('.total_tax', ),
            'DiscountRate': ('.discount_rate', ),

            'LineAmount': (
                lambda shipment: shipment['cost'] + shipment.total_discount, ),
        })


class ToXeroInvoice(_ToXeroTransaction):

    def __init__(self):
        key_field = 'InvoiceID'
        super(ToXeroInvoice, self).__init__(key_field)

        line_conv = ToXeroLineItem()

        self.conversion_map.update({
            'Type': 'ACCREC',

            # Unique alpha numeric code identifying invoice
            'InvoiceNumber': ('id', self._format_invoice_number),

            # another reference id
            'Reference': ('number', ),

            # update full contact info
            'Contact': (ToXeroInvoiceContact(), ),
            # just link to contact
            #'Contact': ('user_id',
            #    lambda user_id: {'ContactNumber': __format_contact_number(user_id)}),

            'Date': ('.effective_date', ),
            'DueDate': ('.payment_due_date', ),

            'LineItems': ('line_items',
                lambda lines: [line_conv(line) for line in lines]),

            'Url': ('.payments_url', ),
            'CurrencyCode': ('currency', ),

            'Status': ('payment_state',
                lambda state: {
                    'paid': 'AUTHORISED',
                    'balance_due': 'AUTHORISED',
                    'credit_owed': 'AUTHORISED',
                    'void': 'VOIDED',
                }.get(state, 'DRAFT')),

            # if the order is paid for, tell Xero the invoice doesn't need to be sent
            'SentToContact': ('payment_state',
                lambda state: state in ['paid', 'credit_owed']),

            #'BrandingThemeID'  See BrandingThemes
        })

    def _post_convert(self, obj, obj_out):
        self._add_extra_lines(obj, obj_out)
        return obj_out

    @staticmethod
    def _format_invoice_number(invoice_id):
        spree_name = configuration.get('Spree', 'name', 'Spree').upper()
        return '{}-Order-{}'.format(spree_name, invoice_id)


class ToXeroPayment(_ToXeroConfigConversion):

    def __init__(self):
        key_field = 'PaymentID'
        super(ToXeroPayment, self).__init__(key_field)

        self.conversion_map.update({
            'Invoice': ('_InvoiceID',
                lambda invoice_id: {'InvoiceID': invoice_id}),
            'Account': (
                lambda p: { 'Code': self.config['payment_account'] }, ),
            'Date': ('created_at', ),
            'Amount': ('amount', ),
            'Reference': ('payment_method', 'name'),
            'IsReconciled': 'false',
            'Status': 'AUTHORISED',
        })


class ToXeroItem(_ToXeroConfigConversion):

    def __init__(self):
        key_field = 'ItemID'
        super(ToXeroItem, self).__init__(key_field)

        self.conversion_map.update({
            'Code': ('sku', self.normalize_sku, ),

            #'InventoryAssetAccountCode'

            'Name': ('.full_name', self._strip_non_ascii,
                lambda name: name if len(name) <= 50 else name[:47] + '...'),
            #'Description': ('description', self._strip_non_ascii, self._strip_tags, ),
            #'PurchaseDetails': (
            #    lambda product: {
            #        #'UnitPrice': ,
            #        'COGSAccountCode': ?,
            #        #'TaxType': 'INPUT',
            #    }, ),
            'SalesDetails': ('price',
                lambda price: {
                    'UnitPrice': price,
                    'AccountCode': self.config['item_account'],
                    'TaxType': 'OUTPUT',
                }),
        })

    @staticmethod
    def normalize_sku(sku):
        return sku.replace('_', '-').replace(' ', '-')

    @staticmethod
    def _strip_non_ascii(text):
        return re.sub(r'[^\x00-\x7F]',' ', text)

    @staticmethod
    def _strip_tags(html):
        text = re.compile(r'<[^>]+>').sub('', html)
        if len(text) > 4000:
            return text[:4000]
        else:
            return text


class ToXeroCreditNote(_ToXeroTransaction):

    def __init__(self):
        key_field = 'CreditNoteID'
        super(ToXeroCreditNote, self).__init__(key_field)

        line_conv = ToXeroLineItem()

        self.conversion_map.update({
            'Type': 'ACCRECCREDIT',
            'Status': 'AUTHORISED',

            # Unique alpha numeric code identifying invoice
            'CreditNoteNumber': ('id', self._format_credit_note_number, ),

            # reference the same order number as the payment
            'Reference': ('_parent', '_parent', 'number', ),

            # just link to contact
            'Contact': ('_parent', '_parent', self._get_contact_from_order, ),

            'Date': ('created_at', ),

            'LineItems': ('_parent', '_parent', 'line_items',
                lambda lines: [line_conv(line) for line in lines]),

            'Url': ('_parent', '_parent', '.payments_url', ),
            'CurrencyCode': ('_parent', '_parent', 'currency', ),

        })

    def _post_convert(self, refund, obj):
        payment = refund['_parent']
        order = payment['_parent']

        if refund['amount'] == order['total']:
            self._add_extra_lines(order, obj)
        else:
            obj['LineItems'] = [{
                'Description': 'Partial Refund',

                'AccountCode': self.config['item_account'],
                'TaxType': 'OUTPUT',

                'Quantity': 1,
                'UnitAmount': refund['amount'],
                'TaxAmount': 0,
                'DiscountRate': 0,
                'LineAmount': refund['amount'],
            }]

        return obj

    @staticmethod
    def _format_credit_note_number(refund_id):
        spree_name = configuration.get('Spree', 'name', 'Spree').upper()
        return '{}-Refund-{}'.format(spree_name, refund_id)

    @staticmethod
    def _get_contact_from_order(order):
        user_id = order.get('user_id', False)

        if user_id:
            contact_number = ToXeroContact._format_contact_number(user_id)
            return { 'ContactNumber': contact_number }
        else:
            contact_id = orientdb.lookup('XeroContact', 'EmailAddress', order.get('email'), 'ContactID')
            return { 'ContactID': contact_id }
